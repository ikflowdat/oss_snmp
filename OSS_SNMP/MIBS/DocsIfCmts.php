<?php

namespace OSS_SNMP\MIBS;

class DocsIfCmts extends \OSS_SNMP\MIB {

    const OID_docsIfCmtsCmStatusMacAddress	= "1.3.6.1.2.1.10.127.1.3.3.1.2";
    //const OID_docsIfCmtsCmStatusTable  		= "1.3.6.1.2.1.10.127.1.3.3";
    const OID_docsIfCmtsCmStatusUpChannelIfIndex =  "1.3.6.1.2.1.10.127.1.3.3.1.5";
    const OID_docsIfCmtsCmStatusDownChannelIfIndex = "1.3.6.1.2.1.10.127.1.3.3.1.4";
    const OID_docsIfCmtsCmStatusIpAddress	= "1.3.6.1.2.1.10.127.1.3.3.1.3";
    const OID_docsQosCmtsIfIndex                = "1.3.6.1.2.1.10.127.7.1.11.1.3";
    const OID_docsQos3CmtsIfIndex		= "1.3.6.1.4.1.4491.2.1.21.1.11.1.3";
    const OID_docsIfCmtsCmStatusIndex		= "1.3.6.1.2.1.10.127.1.3.3.1.1";
    const OID_docsQosServiceFlowOctets		= "1.3.6.1.2.1.10.127.7.1.4.1.2";
    const OID_docsQosServiceFlowDirection	= "1.3.6.1.2.1.10.127.7.1.3.1.7";
    const OID_docsQos3ServiceFlowDirection	= "1.3.6.1.4.1.4491.2.1.21.1.3.1.7";
    const OID_docsIfCmStatusRxPower		= "1.3.6.1.2.1.10.127.1.1.1.1.6";
    const OID_docsIfCmtsCmStatusRxPower		= "1.3.6.1.2.1.10.127.1.3.3.1.6";
    const OID_docsIfCmtsCmStatusValue		= "1.3.6.1.2.1.10.127.1.3.3.1.9";
    const OID_docsQos3ServiceFlowOctets         = "1.3.6.1.4.1.4491.2.1.21.1.4.1.2";
    const OID_docsIfSigQSignalNoise		= "1.3.6.1.2.1.10.127.1.1.4.1.5";
    const OID_docsIfSigQMicroreflections        = "1.3.6.1.2.1.10.127.1.1.4.1.6";
    const docsIfCmtsChannelUtilization          = "1.3.6.1.2.1.10.127.1.3.9.1.3";
    const OID_ipNetToMediaPhysAddress 		= "1.3.6.1.2.1.4.22.1.2";
    const OID_docsIfCmtsCmPtr  			= "1.3.6.1.2.1.10.127.1.3.7.1.2";
    const OID_docsIfCmStatusTxPower 		= "1.3.6.1.2.1.10.127.1.2.2.1.3";
    const OID_docsIfDownChannelFrequency	= "1.3.6.1.2.1.10.127.1.1.1.1.2";
    const OID_docsIfDownChannelModulation       = "1.3.6.1.2.1.10.127.1.1.1.1.4";
    const OID_docsIfDownChannelPower		= "1.3.6.1.2.1.10.127.1.1.1.1.6";
    const OID_docsIfUpChannelFrequency		= "1.3.6.1.2.1.10.127.1.1.2.1.2";
    const OID_docsIfUpChannelWidth 		= "1.3.6.1.2.1.10.127.1.1.2.1.3";
    const OID_docsIfCmtsCmStatusSignalNoise 	= "1.3.6.1.2.1.10.127.1.3.3.1.13";

    const OID_docsQosServiceFlowTimeActive 	= "1.3.6.1.2.1.10.127.7.1.4.1.4";
    const OID_docsQos3ServiceFlowTimeActive 	= "1.3.6.1.4.1.4491.2.1.21.1.4.1.4";

    const OID_docsQosParamSetMaxTrafficRate		= "1.3.6.1.2.1.10.127.7.1.2.2.6";
    const OID_docsQos3ParamSetMaxTrafficRate	= "1.3.6.1.4.1.4491.2.1.21.1.2.1.6";

    public function docsQosParamSetMaxTrafficRate() {

        try{

            $rtr = array();

            $oid = self::OID_docsQosParamSetMaxTrafficRate;

            foreach($this->getSNMP()->realWalk($oid) as $k => $v){
                $k_prim = preg_split("|\.|",$k);
                if ($k_prim[16] == 1) { // Solo activos
                    $rtr[$k_prim[14].$k_prim[15]] = $this->getSNMP()->parseSnmpValue($v);
                }
            }

          return $rtr;

        } catch (\Exception $e) {
          echo "ouch! ".$e->getMessage()."\n";
          return array();
        }
    }

    public function docsQos3ParamSetMaxTrafficRate() {

      	try{

          $rtr = array();

          $oid = self::OID_docsQos3ParamSetMaxTrafficRate;

          foreach($this->getSNMP()->realWalk($oid) as $k => $v){
            $k_prim = preg_split("|\.|",$k);
            if ($k_prim[16] == 1) { // solo activos
                $rtr[$k_prim[15].$k_prim[17]] = $this->getSNMP()->parseSnmpValue($v);
            }
          }

          return $rtr;

        } catch (\Exception $e) {
          echo "ouch! ".$e->getMessage()."\n";
          return array();
        }
    }

    public function docsIfUpChannelWidth(){
		return $this->getSNMP()->walk1d(self::OID_docsIfUpChannelWidth);
    }

    public function docsIfUPChannelFrequency(){
		return $this->getSNMP()->walk1d(self::OID_docsIfUpChannelFrequency);
    }

    public function docsIfDownChannelPower(){
		return $this->getSNMP()->walk1d(self::OID_docsIfDownChannelPower);
    }

    public function docsIfDownChannelModulation($decode = false){
	if(!$decode) {
		return $this->getSNMP()->walk1d(self::OID_docsIfDownChannelModulation);
	}else{
		$trans = array(1 => "unknown", 2 => "other", 3 => "qam64", 4 => "qam256");
		$rtr = array();
		foreach($this->getSNMP()->walk1d(self::OID_docsIfDownChannelModulation) as $if => $val){
			$rtr[$if] = $trans[$val];
		}
		return $rtr;
	}
    }

    public function docsIfDownChannelFrequency(){
	return $this->getSNMP()->walk1d(self::OID_docsIfDownChannelFrequency);
    }

    public function docsIfCmtsCmPtr(){
	$ptr = array();
	foreach($this->getSNMP()->realWalk(self::OID_docsIfCmtsCmPtr) as $key => $val){
		$mac =implode('',array_map(function($x) { return sprintf("%02x", $x);}, array_slice(explode(".",$key),14,6)));
		$ptr[$mac] = $this->getSNMP()->parseSnmpValue($val);
	}
	return $ptr;
    }

    public function ipNetToMediaPhysAddress(){
	$rtr = array();
        foreach($this->getSNMP()->realWalk(self::OID_ipNetToMediaPhysAddress) as $k => $m){
////tring(40) ".1.3.6.1.2.1.4.22.1.2.2000001.10.14.8.69"
//string(23) "STRING: 0:c:e5:7d:93:d0"
////
		$karray = preg_split("|\.|",$k);
		$mac = preg_split("|:|", trim(substr($m,8)));
		$mac = sprintf("%02x:%02x:%02x:%02x:%02x:%02x",$mac[0], $mac[1], $mac[2], $mac[3], $mac[4], $mac[5]);
		$l_ke = count($karray);
		$rtr[$mac] = sprintf("%d.%d.%d.%d",$karray[$l_ke - 4],$karray[$l_ke - 3], $karray[$l_ke - 2], $karray[$l_ke-1]);
	}
	return $rtr;
    }

    public function docsQos3ServiceFlowDirection($translate=false){
	$rtr = array();
	$oid = self::OID_docsQos3ServiceFlowDirection;
	$stats = $this->getSNMP()->realWalk($oid);
        foreach((array)$stats as $k => $v){
	    $k_prim = preg_split("|\.|",$k);
	    //$dds = strpos($v, ':');
	    if(!isset($k_prim[15])){
		continue;
	    }
	    $rtr[$k_prim[15].".".$k_prim[16]] = $this->getSNMP()->parseSnmpValue($v);
	}
	if( !$translate )
            return $rtr;

        return $this->getSNMP()->translate( $rtr, self::$FLOW_DIRECTIONS);
    }

    public function docsQos3ServiceFlowDirectionX($translate=false, $id=null){
	$rtr = array();
	if(is_null($id)) $oid = self::OID_docsQos3ServiceFlowDirection;
	else $oid = self::OID_docsQos3ServiceFlowDirection. "." .$id;
	$stats = $this->getSNMP()->realWalk($oid);
        foreach((array)$stats as $k => $v){
	    $k_prim = preg_split("|\.|",$k);
	    //$dds = strpos($v, ':');
	    if(!isset($k_prim[15])){
		break;
	    }
	    $rtr[$k_prim[15].".".$k_prim[16]] = $this->getSNMP()->parseSnmpValue($v);
	}
	if( !$translate )
            return $rtr;

        return $this->getSNMP()->translate( $rtr, self::$FLOW_DIRECTIONS);
    }



    private function getDecMac($mac){
	preg_match('/^([a-f0-9]{1,2})[-:]*([a-f0-9]{1,2})[-:]*([a-f0-9]{1,2})[-:]*([a-f0-9]{1,2})[-:]*([a-f0-9]{1,2})[-:]*([a-f0-9]{1,2})$/i', $mac, $matches);
	unset($matches[0]);
	return implode('.', array_map('hexdec', $matches));
   }

   public function docsQos3CmtsIfIndex($mac=null){
	    $rtr = array();
	    if(!is_null($mac)){
		$oid = self::OID_docsQos3CmtsIfIndex .'.'.$this->getDecMac($mac);
	    }else{
		$oid = self::OID_docsQos3CmtsIfIndex;
	    }
            foreach((array)$this->getSNMP()->realWalk($oid) as $k => $v){
		$k_prim = preg_split("|\.|",$k);
		if(count($k_prim) < 20){
			continue;
		}
		$k_mac = sprintf("%02x:%02x:%02x:%02x:%02x:%02x",
				 $k_prim[15], $k_prim[16], $k_prim[17], $k_prim[18], $k_prim[19], $k_prim[20]);
		$rtr[$k_mac][] = $this->getSNMP()->parseSnmpValue($v). ".".$k_prim[21];
	    }
	    return $rtr;
   }

   public function docsQos3ServiceFlowOctets($ifId=null){
	try{
	    $rtr = array();
	    if(!is_null($ifId)){
		$oid = self::OID_docsQos3ServiceFlowOctets ."." .$ifId;
	    }else{
		$oid = self::OID_docsQos3ServiceFlowOctets;
            }
	    foreach($this->getSNMP()->realWalk($oid) as $k => $v){
		    $k_prim = preg_split("|\.|",$k);
		    $rtr[$k_prim[15].".".$k_prim[16]] = $this->getSNMP()->parseSnmpValue($v);
	    }
	    return $rtr;
	}catch (\Exception $e){
	    echo "ouch! ".$e->getMessage()."\n";
	    return array();
	}
   }

    public static $FLOW_DIRECTIONS = array(1 => "downstream", 2 => 'upstream');

    public function docsQosServiceFlowDirection($translate=false){
	$oid = self::OID_docsQosServiceFlowDirection;
	$states = $this->getSNMP()->realWalk($oid);
	if(!is_array($states)) return null;
	foreach($states as $k => $v){
		$oid_arr = explode(".", $k);
		$states[$oid_arr[count($oid_arr) -2] . "." . $oid_arr[count($oid_arr) -1]] = $this->getSNMP()->parseSnmpValue($v);
		unset($states[$k]);
	}
	$rtr = array();
	if( !$translate )
            return $states;

        return $this->getSNMP()->translate( $states, self::$FLOW_DIRECTIONS);
    }

    public function docsQosServiceFlowDirectionX($translate=false, $id=null){

	if(is_null($id)) $oid = self::OID_docsQosServiceFlowDirection;
	else $oid = self::OID_docsQosServiceFlowDirection. "." .$id;
	$rtr = array();
	if( !$translate ){
		return $states = $this->getSNMP()->walk1d($oid);
	}
	$states = $this->getSNMP()->realWalk($oid);
	foreach($states as $k => $v){
		$k_p= preg_split("|\.|",$k);
        	$rtr[$k_p[14].".".$k_p[15]] = self::$FLOW_DIRECTIONS[$this->getSNMP()->parseSnmpValue($v)];
	}
	return $rtr;
    }

    public function docsQosCmtsIfIndex($mac=null){
	    $rtr = array();
	    if(!is_null($mac)){
			$oid = self::OID_docsQosCmtsIfIndex .'.'.$this->getDecMac($mac);
	    }else{
			$oid = self::OID_docsQosCmtsIfIndex;
	    }
	    $ifIndex = $this->getSNMP()->realWalk($oid);
        foreach((array)$ifIndex as $k => $v){
			$k_prim = preg_split("|\.|",$k);
			if(!isset($k_prim[14]) OR !isset($k_prim[15]) OR !isset($k_prim[16]) OR !isset($k_prim[17]) OR !isset($k_prim[18]) OR !isset($k_prim[19])){
				continue;
			}
			$k_mac = sprintf("%02x:%02x:%02x:%02x:%02x:%02x", $k_prim[14], $k_prim[15], $k_prim[16], $k_prim[17], $k_prim[18], $k_prim[19]);
		
			$rtr[$k_mac][] = $this->getSNMP()->parseSnmpValue($v).".". $k_prim[20];
		}

	    return $rtr;
    }

    public function docsQosServiceFlowOctets($ifId=null){
		if(!is_null($ifId)){
			$oid = self::OID_docsQosServiceFlowOctets ."." .$ifId;
        }else{
			$oid = self::OID_docsQosServiceFlowOctets;
        }	
		$rtr = array();
		$stats = $this->getSNMP()->realWalk($oid);
		if(!is_array($stats)){
			return null;
		}
		foreach($stats as $k => $v){
            $k_prim = explode(".",$k);
			$rtr[ $k_prim[count($k_prim) - 2] . "." . $k_prim[count($k_prim) - 1]] = $this->getSNMP()->parseSnmpValue($v);
			/*
            if($ifId){
				$k_prim = explode(".",$k);
				$rtr[ $k_prim[count($k_prim) - 2] . "." . $k_prim[count($k_prim) - 1]] = $this->getSNMP()->parseSnmpValue($v);
			}else{
				$k_prim = explode(".",$k);
				$rtr[$k_prim[count($k_prim) - 1]] = $this->getSNMP()->parseSnmpValue($v);
			}
            */

		}
	return $rtr;
    }

    /**
other(1)   Any state other than below.
ranging(2)   The CMTS has received an Initial Ranging Request   message from the CM, and the ranging process is not  yet   complete.
rangingAborted(3)   The CMTS has sent a Ranging Abort message to the CM.
rangingComplete(4)   The CMTS has sent a Ranging Complete message to the CM.
ipComplete(5)   The CMTS has received a DHCP reply message and forwarded   it to the CM.
registrationComplete(6)   The CMTS has sent a Registration Response message to the CM.
accessDenied(7)   The CMTS has sent a Registration Aborted message   to the CM.
      **/
    public function docsIfCmtsCmStatusMacAddress()
    {
        $rtr = $this->getSNMP()->walk1d(self::OID_docsIfCmtsCmStatusMacAddress);
	return $rtr;
    }

    //public function docsIfCmtsCmStatusIndex(){
    //    return $this->getSNMP()->walk1d(self::OID_docsIfCmtsCmStatusDownChannelIfIndex);
    //}

    public function docsIfCmtsCmStatusUpChannelIfIndex(){
    	return $this->getSNMP()->walk1d(self::OID_docsIfCmtsCmStatusUpChannelIfIndex);
    }

    public function docsIfCmtsCmStatusDownChannelIfIndex(){
    	return $this->getSNMP()->walk1d(self::OID_docsIfCmtsCmStatusDownChannelIfIndex);
    }

    public function docsIfCmtsCmStatusIpAddress(){
        return $this->getSNMP()->walk1d(self::OID_docsIfCmtsCmStatusIpAddress);
    }


    public function docsIfCmtsCmStatusValue(){

        $rtr = $this->getSNMP()->walk1d(self::OID_docsIfCmtsCmStatusValue);
        $replace = array(1 => 'other' , 2 =>  'ranging', 3 => 'rangingAborted', 4 => 'rangingComplete', 5 => 'ipComplete',
                        6 => 'registrationComplete', 7 => 'accessDenied');
        foreach($rtr as $k => $v){ if(isset($replace[$v])) {$rtr[$k] = $replace[$v];} }
	return $rtr;

    }

    public function docsIfCmtsCmStatusRxPower(){
	$rtr = $this->getSNMP()->walk1d(self::OID_docsIfCmtsCmStatusRxPower);
	return $rtr;
    }

    public function docsIfCmStatusRxPower(){
	$rtr = $this->getSNMP()->walk1d(self::OID_docsIfCmStatusRxPower);
	return $rtr;
    }

    public function docsIfSigQSignalNoise(){
	$rtr = array();
	$data = $this->getSNMP()->realWalk(self::OID_docsIfSigQSignalNoise);
	if($data) foreach($data as $oid => $val){
		sscanf($val,"INTEGER: %f TenthdB", $readval);
		$rtr[substr($oid,strlen(self::OID_docsIfSigQSignalNoise)+2)] = $readval;
	}
	return $rtr;
    }

    public function docsIfSigQMicroreflections($trans = false){
	$oid = self::OID_docsIfSigQMicroreflections;
	$rtr = array();
	if($trans){
	$data = $this->getSNMP()->realWalk($oid);
	if($data) foreach($data as $oid => $val){
		sscanf($val,"INTEGER: %f -dBc", $readval);
		$rtr[substr($oid,strlen(self::OID_docsIfSigQSignalNoise)+2)] = $readval;
	}
		return $rtr;
	}else{
		return $this->getSNMP()->walk1d(self::OID_docsIfSigQMicroreflections);
	}
    }

    public function docsIfCmtsChannelUtilization(){
	return $this->getSNMP()->realWalk(self::docsIfCmtsChannelUtilization);
    }


    public function docsIfCmStatusTxPower($decode=false){
	if(!$decode) return $this->getSNMP()->walk1d(self::OID_docsIfCmStatusTxPower);
	else{
		$rtr = array();
		foreach($this->getSNMP()->realWalk(self::OID_docsIfCmStatusTxPower) as $_oid => $val){
			$k = substr($_oid, strlen(self::OID_docsIfCmStatusTxPower)+2);
			$rtr[$k] = $this->getSNMP()->parseSnmpValue($val);
		}
		return $rtr;
	}
    }

   public function getCmStatus($index){
	   $mibs = array(
                'docsIfCmtsCmStatusMacAddress' => 'docsIfCmtsCmStatusMacAddress',
                'docsIfCmtsCmStatusIpAddress' => 'docsIfCmtsCmStatusIpAddress',
                'docsIfCmtsCmStatusDownChannelIfIndex' => 'docsIfCmtsCmStatusDownChannelIfIndex',
                'docsIfCmtsCmStatusUpChannelIfIndex' => 'docsIfCmtsCmStatusUpChannelIfIndex',
                //'docsIfCmtsCmStatusRxPower' => '1.3.6.1.2.1.10.127.1.1.1.1.6',
                'docsIfCmtsCmStatusValue' => 'docsIfCmtsCmStatusValue',
                'docsIfCmtsCmStatusUnerroreds' => 'docsIfCmtsCmStatusUnerroreds',
                'docsIfCmtsCmStatusCorrecteds' => 'docsIfCmtsCmStatusCorrecteds',
                'docsIfCmtsCmStatusUncorrectables' => 'docsIfCmtsCmStatusUncorrectables',
                'docsIfCmtsCmStatusSignalNoise' => 'docsIfCmtsCmStatusSignalNoise',
                'docsIfCmtsCmStatusMicroreflections' => 'docsIfCmtsCmStatusMicroreflections',
                'docsIfCmtsCmStatusDocsisRegMode' => 'docsIfCmtsCmStatusDocsisRegMode',
                'docsIfCmtsCmStatusModulationType' => 'docsIfCmtsCmStatusModulationType',
	);

	foreach($mibs as $k=> $m){
		$m .= ".".$index;
		$rta = $this->getSNMP()->realWalk($m);
		if(!$rta) continue;
		$v = current($rta);
		$dds = strpos($v, ':');
		$v = substr($v, $dds+2);
		$rtr[$k] = $v;
	}
	return $rtr;
    }

    public function docsIfCmtsCmStatusSignalNoise(){
	$rtr = array();
	$data = $this->getSNMP()->realWalk(self::OID_docsIfCmtsCmStatusSignalNoise);
        foreach($data as $oid => $val){
		sscanf($val,"INTEGER: %f TenthdB", $readval);
		$rtr[substr($oid,strlen(self::OID_docsIfCmtsCmStatusSignalNoise)+2)] = $readval;
	}
	return $rtr;
    }

    public function docsQos3ServiceFlowTimeActive($ifId = null) {
        try {
            $rtr = array();
            if (!is_null($ifId)) {
                $oid = self::OID_docsQos3ServiceFlowTimeActive . "." . $ifId;
            } else {
                $oid = self::OID_docsQos3ServiceFlowTimeActive;
            }
			$stats = $this->getSNMP()->realWalk($oid);
			
			foreach ($stats as $k => $v) {
                $k_prim = preg_split("|\.|", $k);
                $rtr[$k_prim[15] . "." . $k_prim[16]] = $this->getSNMP()->parseSnmpValue($v);
            }
            
			return $rtr;
        } catch (\Exception $e) {
            echo "ouch! " . $e->getMessage() . "\n";
            return array();
        }
    }

    public function docsQosServiceFlowTimeActive($ifId = null) {
        if (!is_null($ifId)) {
            $oid = self::OID_docsQosServiceFlowTimeActive . "." . $ifId;
        } else {
            $oid = self::OID_docsQosServiceFlowTimeActive;
        }
        $rtr = array();
        $stats = $this->getSNMP()->realWalk($oid);
		
		if (!is_array($stats)) {
            return array();
        }
        
		foreach ($stats as $k => $v) {
			$k_prim = explode(".", $k);
			$rtr[$k_prim[count($k_prim) - 2] . "." . $k_prim[count($k_prim) - 1]] = $this->getSNMP()->parseSnmpValue($v);
            // if ($ifId) {
            //     $k_prim = explode(".", $k);
            //     $rtr[$k_prim[count($k_prim) - 2] . "." . $k_prim[count($k_prim) - 1]] = $this->getSNMP()->parseSnmpValue($v);
            // } else {
            //     $k_prim = explode(".", $k);
            //     $rtr[$k_prim[count($k_prim) - 1]] = $this->getSNMP()->parseSnmpValue($v);
            // }
        }
        return $rtr;
    }

    public function docsQosServiceFlowOctetsRealValues($ifId=null){
        try{

            if(!is_null($ifId)){
                $oid = self::OID_docsQosServiceFlowOctets ."." .$ifId;
            }else{
                $oid = self::OID_docsQosServiceFlowOctets;
            }

            $stats = $this->getSNMP()->realWalk($oid);

            if(!is_array($stats)){return array();}

            return $stats;

        }catch (\Exception $e){
	    echo "ouch! ".$e->getMessage()."\n";
	    return array();
	}
    }

    public function docsQos3ServiceFlowOctetsRealValues($ifId=null){
	try{

	    if(!is_null($ifId)){
		$oid = self::OID_docsQos3ServiceFlowOctets ."." .$ifId;
	    }else{
		$oid = self::OID_docsQos3ServiceFlowOctets;
            }

            $stats = $this->getSNMP()->realWalk($oid);

            if(!is_array($stats)){return array();}

            return $stats;

        }catch (\Exception $e){
	    echo "ouch! ".$e->getMessage()."\n";
	    return array();
	}
   }

}
